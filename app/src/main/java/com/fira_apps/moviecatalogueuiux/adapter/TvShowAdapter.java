package com.fira_apps.moviecatalogueuiux.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fira_apps.moviecatalogueuiux.R;
import com.fira_apps.moviecatalogueuiux.model.TvShow;

import java.util.ArrayList;

public class TvShowAdapter extends RecyclerView.Adapter<TvShowAdapter.TvShowViewHolder> {
    private ArrayList<TvShow> listTvShow = new ArrayList<>();
    private OnItemClickCallback onItemClickCallback;

    public TvShowAdapter() {

    }

    public void setData(ArrayList<TvShow> list) {
        listTvShow.clear();
        listTvShow.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TvShowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);

        return new TvShowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowViewHolder holder, int position) {
        final TvShow tvShow = listTvShow.get(position);
        Glide.with(holder.itemView.getContext())
                .load(tvShow.getPoster())
                .into(holder.poster);
        holder.title.setText(tvShow.getTitle());
        holder.release_information.setText(tvShow.getRelease_information());
        holder.user_score.setText(String.format("Rating: %s", tvShow.getUser_score()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickCallback.onItemClicked(tvShow);
            }
        });
    }

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public interface OnItemClickCallback {
        void onItemClicked(TvShow data);
    }

    @Override
    public int getItemCount() {
        return listTvShow.size();
    }

    public class TvShowViewHolder extends RecyclerView.ViewHolder {
        ImageView poster;
        TextView title, release_information, user_score;

        public TvShowViewHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.poster);
            title = itemView.findViewById(R.id.title);
            release_information = itemView.findViewById(R.id.release_information);
            user_score = itemView.findViewById(R.id.user_score);
        }
    }
}
