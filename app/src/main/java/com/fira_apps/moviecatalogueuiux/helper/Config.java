package com.fira_apps.moviecatalogueuiux.helper;

public class Config {
    public static final String API_KEY = "80986811b097c51cbfa0df0e5fe0419b";
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String GET_MOVIES = BASE_URL + "discover/movie?api_key=" + API_KEY;
    public static final String GET_TV_SHOW = BASE_URL + "discover/tv?api_key=" + API_KEY;
    public static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500";
}
