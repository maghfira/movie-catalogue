package com.fira_apps.moviecatalogueuiux;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fira_apps.moviecatalogueuiux.model.TvShow;
import com.fira_apps.moviecatalogueuiux.viewmodel.TvShowViewModel;

public class DetailTvShowActivity extends AppCompatActivity {
    public static final String EXTRA_TV_SHOW = "extra_tv_show";
    private ImageView poster;
    private TextView title, genres, status, type, original_language, runtime, user_score, overview;
    private TvShowViewModel tvShowViewModel;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tv_show);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setActionBarTitle(getResources().getString(R.string.detail_tv_show));

        poster = findViewById(R.id.poster);
        title = findViewById(R.id.title);
        genres = findViewById(R.id.genres);
        status = findViewById(R.id.status);
        type = findViewById(R.id.type);
        original_language = findViewById(R.id.original_language);
        runtime = findViewById(R.id.runtime);
        user_score = findViewById(R.id.user_score);
        overview = findViewById(R.id.overview);
        progressBar = findViewById(R.id.progressBar);

        tvShowViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(TvShowViewModel.class);

        setData();
    }

    private void setData() {
        showLoading(true);
        TvShow tvShow = getIntent().getParcelableExtra(EXTRA_TV_SHOW);
        tvShowViewModel.getDetailTvShow(this, tvShow.getId()).observe(this, new Observer<TvShow>() {
            @Override
            public void onChanged(TvShow tvShow) {
                Glide.with(DetailTvShowActivity.this)
                        .load(tvShow.getPoster())
                        .into(poster);
                title.setText(tvShow.getTitle());
                genres.setText(tvShow.getGenres());
                status.setText(tvShow.getStatus());
                type.setText(tvShow.getType());
                original_language.setText(tvShow.getOriginal_language());
                runtime.setText(tvShow.getRuntime());
                user_score.setText(tvShow.getUser_score());
                overview.setText(tvShow.getOverview());
                showLoading(false);
            }
        });
    }

    private void setActionBarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}
