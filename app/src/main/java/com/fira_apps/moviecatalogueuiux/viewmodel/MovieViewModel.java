package com.fira_apps.moviecatalogueuiux.viewmodel;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fira_apps.moviecatalogueuiux.model.Movie;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static com.fira_apps.moviecatalogueuiux.helper.Config.API_KEY;
import static com.fira_apps.moviecatalogueuiux.helper.Config.BASE_URL;
import static com.fira_apps.moviecatalogueuiux.helper.Config.GET_MOVIES;
import static com.fira_apps.moviecatalogueuiux.helper.Config.IMAGE_URL;

public class MovieViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Movie>> listMovie = new MutableLiveData<>();
    private MutableLiveData<Movie> detailMovie;

    public void setListMovie(final Context context) {
        //request API
        final ArrayList<Movie> listItems = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        String language = "";
        Log.d("Local Language", Locale.getDefault().getLanguage());
        switch (Locale.getDefault().getLanguage()) {
            case "en":
                language = "en-US";
                break;
            case "in":
                language = "id-ID";
                break;
        }
        String url = GET_MOVIES + "&language=" + language;
        Log.d("url", url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");
                    for (int i = 0; i < list.length(); i++) {
                        JSONObject data = list.getJSONObject(i);
                        Movie movie = new Movie();
                        movie.setId(data.getInt("id"));
                        movie.setTitle(data.getString("title"));
                        movie.setPoster(IMAGE_URL + data.getString("poster_path"));
                        movie.setRelease_information(data.getString("release_date"));
                        movie.setUser_score(data.getString("vote_average"));
                        movie.setOverview(data.getString("overview"));
                        listItems.add(movie);
                    }
                    listMovie.postValue(listItems);
                } catch (Exception e) {
                    Log.d("Exception", "Error saat mengambil data movie : " + e.getMessage());
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", "Error saat mengambil data movie : " + error.getMessage());
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public LiveData<ArrayList<Movie>> getListMovie() {
        return listMovie;
    }

    public LiveData<Movie> getDetailMovie(Context context, int id) {
        if (detailMovie == null) {
            detailMovie = new MutableLiveData<>();
            loadDetailMovie(context, id);
        }
        return detailMovie;
    }

    private void loadDetailMovie(final Context context, int id) {
        AsyncHttpClient client = new AsyncHttpClient();
        String language = "";
        switch (Locale.getDefault().getLanguage()) {
            case "en":
                language = "en-US";
                break;
            case "in":
                language = "id-ID";
                break;
        }
        String url = BASE_URL + "movie/" + id + "?api_key=" + API_KEY + "&language=" + language;
        Log.d("url", url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    Movie movie = new Movie();
                    movie.setId(responseObject.getInt("id"));
                    movie.setTitle(responseObject.getString("title"));
                    movie.setPoster(IMAGE_URL + responseObject.getString("poster_path"));
                    movie.setStatus(responseObject.getString("status"));
                    movie.setUser_score(responseObject.getString("vote_average"));
                    movie.setOverview(responseObject.getString("overview"));
                    movie.setRelease_information(responseObject.getString("release_date"));
                    movie.setRuntime(responseObject.getString("runtime") + "m");
                    movie.setOriginal_language(responseObject.getString("original_language"));
                    StringBuilder genres = new StringBuilder();
                    JSONArray dataGenres = responseObject.getJSONArray("genres");
                    for (int i = 0; i< dataGenres.length(); i++) {
                        JSONObject genre = dataGenres.getJSONObject(i);
                        if (i == 0) {
                            genres.append(genre.getString("name"));
                        } else {
                            genres.append(", ").append(genre.getString("name"));
                        }
                    }
                    movie.setGenres(genres.toString());
                    detailMovie.postValue(movie);
                } catch (Exception e) {
                    Log.d("Exception", "Error saat mengambil data detail movie : " + e.getMessage());
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", "Error saat mengambil data detail movie : " + error.getMessage());
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
