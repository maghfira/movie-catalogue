package com.fira_apps.moviecatalogueuiux.viewmodel;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fira_apps.moviecatalogueuiux.model.TvShow;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static com.fira_apps.moviecatalogueuiux.helper.Config.API_KEY;
import static com.fira_apps.moviecatalogueuiux.helper.Config.BASE_URL;
import static com.fira_apps.moviecatalogueuiux.helper.Config.GET_TV_SHOW;
import static com.fira_apps.moviecatalogueuiux.helper.Config.IMAGE_URL;

public class TvShowViewModel extends ViewModel {
    private MutableLiveData<ArrayList<TvShow>> listTvShow = new MutableLiveData<>();
    private MutableLiveData<TvShow> detailTvShow;

    public void setListTvShow(final Context context) {
        //request API TV Show
        final ArrayList<TvShow> listItems = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        String language = "";
        switch (Locale.getDefault().getLanguage()) {
            case "en":
                language = "en-US";
                break;
            case "in":
                language = "id-ID";
                break;
        }
        String url = GET_TV_SHOW + "&language=" + language;
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");
                    for (int i = 0; i< list.length(); i++) {
                        JSONObject data = list.getJSONObject(i);
                        TvShow tvShow =  new TvShow();
                        tvShow.setId(data.getInt("id"));
                        tvShow.setTitle(data.getString("name"));
                        tvShow.setPoster(IMAGE_URL + data.getString("poster_path"));
                        tvShow.setRelease_information(data.getString("first_air_date"));
                        tvShow.setUser_score(data.getString("vote_average"));
                        tvShow.setOverview(data.getString("overview"));
                        listItems.add(tvShow);
                    }
                    listTvShow.postValue(listItems);
                } catch (Exception e) {
                    Log.d("Exception", "Error saat mengambil data TV Show : " + e.getMessage());
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", "Error saat mengambil data TV Show : " + error.getMessage());
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public LiveData<ArrayList<TvShow>> getListTvShow() {
        return listTvShow;
    }

    public LiveData<TvShow> getDetailTvShow(Context context, int id) {
        if (detailTvShow == null) {
            detailTvShow = new MutableLiveData<>();
            loadDetailTvShow(context, id);
        }
        return detailTvShow;
    }

    private void loadDetailTvShow(final Context context, int id) {
        AsyncHttpClient client = new AsyncHttpClient();
        String language = "";
        switch (Locale.getDefault().getLanguage()) {
            case "en":
                language = "en-US";
                break;
            case "in":
                language = "id-ID";
                break;
        }
        String url = BASE_URL + "tv/" + id + "?api_key=" + API_KEY + "&language=" + language;
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    TvShow tvShow = new TvShow();
                    tvShow.setId(responseObject.getInt("id"));
                    tvShow.setPoster(IMAGE_URL + responseObject.getString("poster_path"));
                    tvShow.setTitle(responseObject.getString("name"));
                    tvShow.setStatus(responseObject.getString("status"));
                    tvShow.setType(responseObject.getString("type"));
                    tvShow.setRelease_information(responseObject.getString("first_air_date"));
                    StringBuilder genres = new StringBuilder();
                    JSONArray dataGenres = responseObject.getJSONArray("genres");
                    for (int i = 0; i< dataGenres.length(); i++) {
                        JSONObject genre = dataGenres.getJSONObject(i);
                        if (i == 0) {
                            genres.append(genre.getString("name"));
                        } else {
                            genres.append(", ").append(genre.getString("name"));
                        }
                    }
                    tvShow.setGenres(genres.toString());
                    tvShow.setOriginal_language(responseObject.getString("original_language"));
                    StringBuilder sbRuntime = new StringBuilder();
                    JSONArray dataRuntime = responseObject.getJSONArray("episode_run_time");
                    for (int i = 0; i < dataRuntime.length(); i++) {
                        if (i == 0) {
                            sbRuntime.append(dataRuntime.get(i)).append("m");
                        } else {
                            sbRuntime.append(",").append(dataRuntime.get(i)).append("m");
                        }
                    }
                    tvShow.setRuntime(sbRuntime.toString());
                    tvShow.setUser_score(responseObject.getString("vote_average"));
                    tvShow.setOverview(responseObject.getString("overview"));
                    detailTvShow.postValue(tvShow);
                } catch (Exception e) {
                    Log.d("Exception", "Error saat mengambil data detail TV Show : " + e.getMessage());
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", "Error saat mengambil data detail TV Show : " + error.getMessage());
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
