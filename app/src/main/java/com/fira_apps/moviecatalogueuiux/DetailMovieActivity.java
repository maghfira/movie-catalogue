package com.fira_apps.moviecatalogueuiux;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fira_apps.moviecatalogueuiux.model.Movie;
import com.fira_apps.moviecatalogueuiux.viewmodel.MovieViewModel;

public class DetailMovieActivity extends AppCompatActivity {
    public static final String EXTRA_MOVIE = "extra_movie";
    private ImageView poster;
    private TextView title, genres, status, release_information, original_language, runtime, user_score, overview;
    private MovieViewModel movieViewModel;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setActionBarTitle(getResources().getString(R.string.detail_movie));

        poster = findViewById(R.id.poster);
        title = findViewById(R.id.title);
        genres = findViewById(R.id.genres);
        status = findViewById(R.id.status);
        release_information = findViewById(R.id.release_information);
        original_language = findViewById(R.id.original_language);
        runtime = findViewById(R.id.runtime);
        user_score = findViewById(R.id.user_score);
        overview = findViewById(R.id.overview);
        progressBar = findViewById(R.id.progressBar);

        movieViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(MovieViewModel.class);

        setData();
    }

    private void setData() {
        showLoading(true);
        Movie movie = getIntent().getParcelableExtra(EXTRA_MOVIE);
        movieViewModel.getDetailMovie(this, movie.getId()).observe(this, new Observer<Movie>() {
            @Override
            public void onChanged(Movie data) {
                if (data != null) {
                    Glide.with(DetailMovieActivity.this)
                            .load(data.getPoster())
                            .into(poster);
                    title.setText(data.getTitle());
                    release_information.setText(data.getRelease_information());
                    user_score.setText(data.getUser_score());
                    overview.setText(data.getOverview());
                    genres.setText(data.getGenres());
                    status.setText(data.getStatus());
                    original_language.setText(data.getOriginal_language());
                    runtime.setText(data.getRuntime());
                    showLoading(false);
                }
            }
        });
    }

    private void setActionBarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}
