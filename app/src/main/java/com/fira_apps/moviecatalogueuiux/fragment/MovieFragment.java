package com.fira_apps.moviecatalogueuiux.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.fira_apps.moviecatalogueuiux.DetailMovieActivity;
import com.fira_apps.moviecatalogueuiux.R;
import com.fira_apps.moviecatalogueuiux.adapter.MovieAdapter;
import com.fira_apps.moviecatalogueuiux.model.Movie;
import com.fira_apps.moviecatalogueuiux.viewmodel.MovieViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment {
    private RecyclerView rvMovie;
    private ProgressBar progressBar;
    private MovieViewModel movieViewModel;


    public MovieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        rvMovie = view.findViewById(R.id.rvMovie);
        progressBar = view.findViewById(R.id.progressBar);

        rvMovie.setLayoutManager(new LinearLayoutManager(getActivity()));
        final MovieAdapter movieAdapter = new MovieAdapter();
        movieAdapter.notifyDataSetChanged();
        rvMovie.setAdapter(movieAdapter);

        movieViewModel = new ViewModelProvider(getActivity(), new ViewModelProvider.NewInstanceFactory()).get(MovieViewModel.class);
        movieViewModel.setListMovie(getActivity());
        showLoading(true);
        movieViewModel.getListMovie().observe(getActivity(), new Observer<ArrayList<Movie>>() {
            @Override
            public void onChanged(ArrayList<Movie> list) {
                if (list != null) {
                    movieAdapter.setData(list);
                    showLoading(false);
                }
            }
        });

        movieAdapter.setOnItemClickCallback(new MovieAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(Movie data) {
                showDetailMovie(data);
            }
        });

        return view;
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void showDetailMovie(Movie data) {
        Intent gotodetailmovie = new Intent(getActivity(), DetailMovieActivity.class);
        gotodetailmovie.putExtra(DetailMovieActivity.EXTRA_MOVIE, data);
        startActivity(gotodetailmovie);
    }

}