package com.fira_apps.moviecatalogueuiux.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.fira_apps.moviecatalogueuiux.DetailTvShowActivity;
import com.fira_apps.moviecatalogueuiux.R;
import com.fira_apps.moviecatalogueuiux.adapter.TvShowAdapter;
import com.fira_apps.moviecatalogueuiux.model.TvShow;
import com.fira_apps.moviecatalogueuiux.viewmodel.TvShowViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TvShowFragment extends Fragment {
    private RecyclerView rvTvShow;
    private ProgressBar progressBar;
    private TvShowViewModel tvShowViewModel;

    public TvShowFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tv_show, container, false);
        rvTvShow = view.findViewById(R.id.rv_tv_show);
        progressBar = view.findViewById(R.id.progressBar);

        rvTvShow.setLayoutManager(new LinearLayoutManager(getActivity()));
        final TvShowAdapter tvShowAdapter = new TvShowAdapter();
        tvShowAdapter.notifyDataSetChanged();
        rvTvShow.setAdapter(tvShowAdapter);

        tvShowViewModel = new ViewModelProvider(getActivity(), new ViewModelProvider.NewInstanceFactory()).get(TvShowViewModel.class);
        tvShowViewModel.setListTvShow(getActivity());
        showLoading(true);
        tvShowViewModel.getListTvShow().observe(getActivity(), new Observer<ArrayList<TvShow>>() {
            @Override
            public void onChanged(ArrayList<TvShow> tvShows) {
                if (tvShows != null) {
                    tvShowAdapter.setData(tvShows);
                    showLoading(false);
                }
            }
        });

        tvShowAdapter.setOnItemClickCallback(new TvShowAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(TvShow data) {
                Intent gotodetailtvshow = new Intent(getActivity(), DetailTvShowActivity.class);
                gotodetailtvshow.putExtra(DetailTvShowActivity.EXTRA_TV_SHOW, data);
                startActivity(gotodetailtvshow);
            }
        });

        return view;
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}
