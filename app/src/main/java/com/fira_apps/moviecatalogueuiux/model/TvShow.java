package com.fira_apps.moviecatalogueuiux.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TvShow implements Parcelable {
    private int id;
    private String poster;
    private String title;
    private String status;
    private String release_information;
    private String original_language;
    private String runtime;
    private String genres;
    private String user_score;
    private String overview;
    private String type;

    public TvShow() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRelease_information() {
        return release_information;
    }

    public void setRelease_information(String release_information) {
        this.release_information = release_information;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getUser_score() {
        return user_score;
    }

    public void setUser_score(String user_score) {
        this.user_score = user_score;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    private TvShow(Parcel in) {
        id = in.readInt();
        poster = in.readString();
        title = in.readString();
        status = in.readString();
        release_information = in.readString();
        original_language = in.readString();
        runtime = in.readString();
        genres = in.readString();
        user_score = in.readString();
        overview = in.readString();
        type = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(poster);
        parcel.writeString(title);
        parcel.writeString(status);
        parcel.writeString(release_information);
        parcel.writeString(original_language);
        parcel.writeString(runtime);
        parcel.writeString(genres);
        parcel.writeString(user_score);
        parcel.writeString(overview);
        parcel.writeString(type);
    }

    public static final Creator<TvShow> CREATOR = new Creator<TvShow>() {
        @Override
        public TvShow createFromParcel(Parcel parcel) {
            return new TvShow(parcel);
        }

        @Override
        public TvShow[] newArray(int i) {
            return new TvShow[i];
        }
    };
}
